import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'Modelos.dart';

class DatabaseHelper {
  //Create a private constructor
  DatabaseHelper._();

  static const databaseName = 'base10.db';
  static final DatabaseHelper instance = DatabaseHelper._();
  static Database _database;

  Future<Database> get database async {
    if (_database == null) {
      return await initializeDatabase();
    }
    return _database;
  }

  initializeDatabase() async {
    return await openDatabase(join(await getDatabasesPath(), databaseName),
        version: 1, onCreate: (Database db, int version) async {
      await db.execute(
          "CREATE TABLE Usuarios(idUsuario INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, clave TEXT, email TEXT UNIQUE, monedero INTEGER)");
      await db.execute(
        "CREATE TABLE Productos(idProducto INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, desc TEXT, precio NUMBER,imagenUrl TEXT, cantidad INTENGER,direccion TEXT,categoria TEXT, idUsuario INTEGER,FOREIGN KEY(idUsuario) REFERENCES Usuarios(idUsuario))",
      );
      await db.execute(
          "CREATE TABLE Carrito(idCarrito INTEGER PRIMARY KEY AUTOINCREMENT,idUsuario INTEGER, idProducto INTEGER, FOREIGN KEY (idProducto) REFERENCES Productos(idProducto))");
    });
  }

  Future<void> insertUsuario(Usuarios usuarios) async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    // Inserta el Dog en la tabla correcta. También puede especificar el
    // `conflictAlgorithm` para usar en caso de que el mismo Dog se inserte dos veces.
    // En este caso, reemplaza cualquier dato anterior.
    await db.insert(
      'Usuarios',
      usuarios.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertCarrito(Carrito carrito) async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    // Inserta el Dog en la tabla correcta. También puede especificar el
    // `conflictAlgorithm` para usar en caso de que el mismo Dog se inserte dos veces.
    // En este caso, reemplaza cualquier dato anterior.
    await db.insert(
      'Carrito',
      carrito.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertProductos(Productos productos) async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    await db.insert(
      'Productos',
      productos.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Productos>> listaProductos() async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    // Consulta la tabla por todos los Dogs.
    final List<Map<String, dynamic>> maps = await db.query('Productos');

    // Convierte List<Map<String, dynamic> en List<Dog>.
    return List.generate(maps.length, (i) {
      return Productos(
          idProducto: maps[i]['idProducto'],
          nombre: maps[i]['nombre'],
          desc: maps[i]['desc'],
          cantidad: maps[i]['cantidad'],
          precio: double.parse(maps[i]['precio'].toString()),
          imagenUrl: maps[i]['imagenUrl'].toString(),
          categoria: maps[i]['categoria'],
          direccion: maps[i]['direccion'],
          idUsuario: maps[i]['idUsuario']);
    });
  }

  Future<List<Productos>> listaProductosEspcifico(int idProducto) async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    // Consulta la tabla por todos los Dogs.
    final List<Map<String, dynamic>> maps = await db
        .query('Productos', where: 'idProducto = ? ', whereArgs: [idProducto]);

    // Convierte List<Map<String, dynamic> en List<Dog>.
    return List.generate(maps.length, (i) {
      return Productos(
        idProducto: maps[i]['idProducto'],
        nombre: maps[i]['nombre'],
        precio: double.parse(maps[i]['precio'].toString()),
        imagenUrl: maps[i]['imagenUrl'].toString(),
      );
    });
  }

  Future<List<Productos>> busquedaProductos(String nombre) async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    // Consulta la tabla por todos los Dogs.
    final List<Map<String, dynamic>> maps =
        await db.query('Productos', where: 'nombre = ? ', whereArgs: [nombre]);

    // Convierte List<Map<String, dynamic> en List<Dog>.
    return List.generate(maps.length, (i) {
      return Productos(
          idProducto: maps[i]['idProducto'],
          nombre: maps[i]['nombre'],
          desc: maps[i]['desc'],
          cantidad: maps[i]['cantidad'],
          precio: double.parse(maps[i]['precio'].toString()),
          imagenUrl: maps[i]['imagenUrl'].toString(),
          categoria: maps[i]['categoria'],
          direccion: maps[i]['direccion'],
          idUsuario: maps[i]['idUsuario']);
    });
  }

  Future<List<Carrito>> listaCarrito(int idUsuario) async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    final List<Map<String, dynamic>> maps = await db
        .query('Carrito', where: "idUsuario = ?", whereArgs: [idUsuario]);

    // Convierte List<Map<String, dynamic> en List<Dog>.
    return List.generate(maps.length, (i) {
      return Carrito(
          idCarrito: maps[i]['idCarrito'],
          idProducto: maps[i]['idProducto'],
          idUsuario: maps[i]['idUsuario']);
    });
  }

  Future<List<Usuarios>> listaUsuarios() async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    // Consulta la tabla por todos los Dogs.
    final List<Map<String, dynamic>> maps = await db.query('Usuarios');

    // Convierte List<Map<String, dynamic> en List<Dog>.
    return List.generate(maps.length, (i) {
      return Usuarios(
        idUsuario: maps[i]['idUsuario'],
        nombre: maps[i]['nombre'],
        eMail: maps[i]['email'],
        clave: maps[i]['clave'],
        monedero: double.parse(maps[i]['monedero'].toString()),
      );
    });
  }

  Future<List<Usuarios>> buscarUsuarios(String email, String clave) async {
    // Obtiene una referencia de la base de datos
    final Database db = await database;

    // Consulta la tabla por todos los Dogs.
    final List<Map<String, dynamic>> maps = await db.query('Usuarios',
        where: 'email = ? AND clave = ?', whereArgs: [email, clave]);

    // Convierte List<Map<String, dynamic> en List<Dog>.
    return List.generate(maps.length, (i) {
      return Usuarios(
        idUsuario: maps[i]['idUsuario'],
        nombre: maps[i]['nombre'],
        eMail: maps[i]['email'],
        clave: maps[i]['clave'],
        monedero: double.parse(maps[i]['monedero'].toString()),
      );
    });
  }

  Future<void> deleteProducto(int id) async {
    // Obtiene una referencia de la base de datos
    final db = await database;

    await db.delete(
      'Carrito',
      where: "idProducto = ?",
      whereArgs: [id],
    );
    await db.delete(
      'Productos',
      // Utiliza la cláusula `where` para eliminar un dog específico
      where: "idProducto = ?",
      whereArgs: [id],
    );
  }

  Future<void> deleteCarrito(int id) async {
    // Obtiene una referencia de la base de datos
    final db = await database;

    // Elimina el Dog de la base de datos
    await db.delete(
      'Carrito',
      // Utiliza la cláusula `where` para eliminar un dog específico
      where: "idCarrito = ?",
      whereArgs: [id],
    );
  }

  Future<void> deleteTodosCarrito(int id) async {
    // Obtiene una referencia de la base de datos
    final db = await database;

    // Elimina el Dog de la base de datos
    await db.delete(
      'Carrito',
      // Utiliza la cláusula `where` para eliminar un dog específico
      where: "idUsuario = ?",
      whereArgs: [id],
    );
  }

  Future<String> obtenerId(String email) async {
    String resultado;
    // get a reference to the database
    Database db = await database;

    // raw query
    List<Map> result = await db
        .rawQuery('SELECT idUsuario FROM usuarios WHERE email=?', [email]);

    // print the result

    result.forEach((row) {
      resultado = row['idUsuario'].toString();
    });

    return resultado;

    // {_id: 2, name: Mary, age: 32}
  }

  Future<void> incrementarCantidad(int idP) async {
    // get a reference to the database
    Database db = await database;

    await db.rawUpdate(
        'UPDATE Productos SET cantidad = cantidad + 1 WHERE idProducto=?',
        [idP]);
  }

  Future<void> decrementarCantidad(int idP) async {
    // get a reference to the database
    Database db = await database;

    await db.rawUpdate(
        'UPDATE Productos SET cantidad = cantidad - 1 WHERE idProducto=?',
        [idP]);
  }

  Future<String> obtenerMonedero(String email) async {
    String resultado;
    // get a reference to the database
    Database db = await database;

    // raw query
    List<Map> result = await db
        .rawQuery('SELECT monedero FROM usuarios WHERE email=?', [email]);

    // print the result

    result.forEach((row) {
      resultado = row['monedero'].toString();
    });

    return resultado;
  }

  Future<void> updateMonedero(Monedero usuarios) async {
    final db = await database;
    await db.update(
      'Usuarios',
      usuarios.toMap(),
      // Aseguúrate de que solo actualizarás el Dog con el id coincidente
      where: "idUsuario = ?",
      whereArgs: [usuarios.idUsuario],
    );
  }

  Future<void> updateProducto(Productos producto) async {
    final db = await database;

    await db.update(
      'Productos',
      producto.toMap(),
      where: "idProducto = ?",
      whereArgs: [producto.idProducto],
    );
  }
}
