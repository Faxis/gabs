import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:gabs/Pages/Database.dart';
import 'package:toast/toast.dart';

import '../Modelos.dart';
import 'funciones.dart';

class VerderPage extends StatefulWidget {
  final String usuario;
  final String id;
  final double monedero;

  VerderPage({Key key, this.usuario, this.id, this.monedero})
      : super(key: key);

  @override
  _VerderPageState createState() => _VerderPageState();
}

class _VerderPageState extends State<VerderPage> {
  List valorItem = <String>[
    'Ropa',
    'Calzado',
    'Electronica',
    'Joyería',
    'Alimentos',
    'Deportes',
    'Belleza',
    'Accesorios',
    'Mueblería'
  ];

  String dropdownValue = 'Ropa';

  TextEditingController nombre = new TextEditingController();
  TextEditingController desc = new TextEditingController();
  TextEditingController precio = new TextEditingController();
  TextEditingController imagenUrl = new TextEditingController();
  TextEditingController cantidad = new TextEditingController();
  TextEditingController direccion = new TextEditingController();

/* Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );
 */

  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          //title: Text('Bienvenido'),
          actions: <Widget>[
            FlatButton.icon(
                onPressed: null,
                icon: Icon(Icons.person_pin),
                label: Text('${widget.usuario}')),
            FlatButton.icon(
                onPressed: null,
                icon: Icon(Icons.monetization_on),
                label: Text(numberFormat(widget.monedero).toString()))
          ],
        ),
        body: contenidoPrincipal(context));
  }

  Container contenidoPrincipal(BuildContext context) {
    
    return Container(
        color: Colors.indigo[50],
        child: ListView(
          children: <Widget>[
            Container(
                margin: EdgeInsets.all(12),
                padding: EdgeInsets.all(12),
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    imagenVender(context),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: nombre,
                        decoration: InputDecoration(
                          hintText: 'Nombre de Producto',
                          labelText: 'Producto',
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        keyboardType: TextInputType.multiline,
                        //expands: true,
                        minLines: 3,
                        maxLines: 8,
                        controller: desc,

                        decoration: InputDecoration(
                          hintText: 'Descripción de Producto',
                          labelText: 'Descripción',
                          prefixIcon: Icon(Icons.description),
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: precio,
                              decoration: InputDecoration(
                                hintText: 'Precio de producto',
                                labelText: 'Precio',
                                prefixIcon: Icon(Icons.monetization_on),
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextField(
                              controller: cantidad,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                hintText: 'Cantidad de productos',
                                labelText: 'Cantidad',
                                prefixIcon: Icon(Icons.keyboard_arrow_up),
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        keyboardType: TextInputType.url,
                        controller: imagenUrl,
                        decoration: InputDecoration(
                          hintText: 'Url de Imagen',
                          labelText: 'Imagen',
                          prefixIcon: Icon(Icons.image),
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DropdownButton<String>(
                        hint: Text('Seleccionar Categoría'),
                        isExpanded: true,
                        value: dropdownValue,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 16,
                        elevation: 2,
                        style: TextStyle(color: Colors.blue[700]),
                        underline: Container(
                          height: 2,
                          color: Colors.blue[700],
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                        items: valorItem.map<DropdownMenuItem<String>>((value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: direccion,
                        decoration: InputDecoration(
                          hintText: 'Direccion',
                          labelText: 'Localizacion del producto',
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    Divider(),
                    FlatButton.icon(
                        color: Colors.cyan[400],
                        onPressed: () {
                          DatabaseHelper.instance.insertProductos(Productos(
                            nombre: nombre.text,
                            desc: desc.text,
                            precio: double.parse(precio.text),
                            cantidad: int.parse(cantidad.text),
                            imagenUrl: imagenUrl.text,
                            categoria: dropdownValue,
                            direccion: direccion.text,
                            idUsuario: int.parse(widget.id)
                          ));

                          Toast.show(
                              "Agregado Exitosamente", context,
                              duration: 3,
                              gravity: Toast.BOTTOM);

                          Navigator.pop(context);
                        },
                        icon: Icon(Icons.add),
                        label: Text('Agregar producto',
                            style: TextStyle(color: Colors.black54)))
                  ],
                ))
          ],
        ));
  }

  Padding imagenVender(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Image.asset('assets/vender.png',
          width: MediaQuery.of(context).size.width * 0.3),
    );
  }
}
