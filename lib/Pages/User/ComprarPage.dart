import 'package:flutter/material.dart';
import 'package:gabs/Pages/Database.dart';
import 'package:gabs/Pages/Modelos.dart';
import 'package:toast/toast.dart';

import 'EditarPage.dart';
import 'funciones.dart';

class ComprarPage extends StatefulWidget {
  final String usuario;
  final String id;
  final double monedero;
  ComprarPage({Key key, this.usuario, this.id, this.monedero})
      : super(key: key);

  @override
  _ComprarPageState createState() => _ComprarPageState();
}

class _ComprarPageState extends State<ComprarPage> {
  ScrollController controller;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: Text('Bienvenido'),
        actions: <Widget>[
          FlatButton.icon(
              onPressed: null,
              icon: Icon(Icons.person_pin),
              label: Text('${widget.usuario}')),
          FlatButton.icon(
              onPressed: null,
              icon: Icon(Icons.monetization_on),
              label: Text(numberFormat(widget.monedero).toString()))
        ],
      ),
      body: Container(
        color: Colors.indigo[50],
        child: FutureBuilder(
          future: DatabaseHelper.instance.listaProductos(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }

            return ListView.builder(
              controller: controller,
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                int cantidadD = snapshot.data[index].cantidad;
                return Container(
                  margin: EdgeInsets.all(4),
                  padding: EdgeInsets.all(4),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        leading: FadeInImage.assetNetwork(
                            placeholder: 'assets/cargando.gif',
                            image: '${snapshot.data[index].imagenUrl}'),
                        title: Text('${snapshot.data[index].nombre}'),
                        subtitle: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('${snapshot.data[index].categoria}'),
                            Divider(),
                            Text('${snapshot.data[index].desc} '),
                            Divider(),
                            Text('${snapshot.data[index].direccion}'),
                            Flex(
                              direction: Axis.horizontal,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                FlatButton.icon(
                                    onPressed: null,
                                    icon: Icon(Icons.attach_money),
                                    label:
                                        Text('${snapshot.data[index].precio}')),
                                Expanded(
                                  child: FlatButton.icon(
                                      onPressed: null,
                                      icon: Icon(Icons.call_to_action),
                                      label: Text(
                                          '${snapshot.data[index].cantidad}')),
                                ),
                              ],
                            )
                          ],
                        ),
                        onTap: () {
                          //snapshot.data[index].cantidad
                          if (cantidadD < 1) {
                            Toast.show("No hay producto disponible", context,
                                duration: 3, gravity: Toast.BOTTOM);
                          } else {
                            DatabaseHelper.instance.insertCarrito(Carrito(
                                idProducto: int.parse(
                                    snapshot.data[index].idProducto.toString()),
                                idUsuario: int.parse(widget.id)));

                            DatabaseHelper.instance.decrementarCantidad(
                                snapshot.data[index].idProducto);
                            Toast.show("Agregado al carrito", context,
                                duration: 2, gravity: Toast.BOTTOM);

                            setState(() {
                              cantidadD--;
                            });
                            //Navigator.pop(context);
                          }
                        },
                        trailing: Icon(Icons.add_shopping_cart),
                      ),
                      snapshot.data[index].idUsuario == int.parse(widget.id)
                          ? opcionesCrud(context, snapshot, index)
                          : Text('No puedo editar este producto')
                    ],
                  ),
                );
              },
            );
          },
        ),

        //paraVisualizar(),
      ),
    );
  }

  Row opcionesCrud(BuildContext context, AsyncSnapshot snapshot, int index) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        FlatButton.icon(
            onPressed: () {
              //snapshot.data[index].

              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => EditarPage(
                          idProducto: '${snapshot.data[index].idProducto}',
                          usuario: widget.usuario,
                          monedero: widget.monedero,
                          id: widget.id.toString(),
                          nombre: snapshot.data[index].nombre,
                          cantidad: '${snapshot.data[index].cantidad}',
                          desc: snapshot.data[index].desc as String,
                          categoria: snapshot.data[index].categoria as String,
                          direccion: snapshot.data[index].direccion as String,
                          imagenUrl: snapshot.data[index].imagenUrl as String,
                          precio: '${snapshot.data[index].precio}',
                        )),
              );
            },
            icon: Icon(Icons.create),
            label: Text('Editar'),
            color: Colors.orange[400]),
        SizedBox(
          width: 4.0,
        ),
        FlatButton.icon(
          onPressed: () {
            _showMyDialog(
                snapshot.data[index].idProducto, snapshot.data[index].nombre);
          },
          icon: Icon(Icons.delete),
          label: Text('Eliminar'),
          color: Colors.red[400],
        )
      ],
    );
  }

  /*  Row opcionesCRUD(int idProducto, String nombre) {
    return ;
  } */

  Future<void> _showMyDialog(int idProducto, String nombre) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Advertencia'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('¿Desea eliminar el producto? '),
                Text(
                  '$nombre',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Si! Eliminar'),
              onPressed: () {
                DatabaseHelper.instance.deleteProducto(idProducto);
                Navigator.of(context).pop();
                Navigator.of(context).pop();
                Toast.show("Eliminado Exitosamente", context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
              },
            ),
          ],
        );
      },
    );
  }
}
