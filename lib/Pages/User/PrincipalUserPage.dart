import 'package:flutter/material.dart';
import 'package:gabs/Pages/User/CarritoPage.dart';
import 'package:gabs/Pages/User/ComprarPage.dart';
import 'package:gabs/Pages/User/VerderPage.dart';

import '../DataBase.dart';
import 'BusquedaPage.dart';
import 'CarteraPage.dart';
import 'funciones.dart';

class PrincipalUserPage extends StatefulWidget {
  final String usuario;
  final String eMail;
  PrincipalUserPage({Key key, this.usuario, this.eMail}) : super(key: key);

  @override
  _PrincipalUserPageState createState() => _PrincipalUserPageState();
}

class _PrincipalUserPageState extends State<PrincipalUserPage> {
  String dataId, dataMonedero;
  TextEditingController controller = new TextEditingController();

  //6ScrollController controller = new ScrollController();

  Future idUsuarioData() async {
    var id = await DatabaseHelper.instance.obtenerId(widget.eMail);
    var monedero = await DatabaseHelper.instance.obtenerMonedero(widget.eMail);
    if (this.mounted) {
      setState(() {
        dataId = id;
        dataMonedero = monedero;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    idUsuarioData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: Text('Bienvenido'),
        actions: <Widget>[
          FlatButton.icon(
              onPressed: null,
              icon: Icon(Icons.person_pin),
              label: Text('${widget.usuario}')),
          FlatButton.icon(
            onPressed: null,
            icon: Icon(Icons.monetization_on),
            label: Text(numberFormat(double.parse(dataMonedero)).toString()),
          )
        ],
      ),
      body: Container(
        color: Colors.indigo[50],
        child:

            //listaUsuarios()
            //Este es todo el menu
            todoContenido(context),
      ),
    );
  }

/* 
  FutureBuilder<List<Usuarios>> listaUsuarios() {
    idUsuarioData();
    return FutureBuilder(
      future: DatabaseHelper.instance.listaUsuarios(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        }

        return ListView.builder(
          controller: controller,
          itemCount: snapshot.data.length,
          itemBuilder: (context, index) {
            //print("Imagen: ${snapshot.data[index].nombre}");
            //print("Direccion: ${snapshot.data[index].monedero}");
            return Container(
              margin: EdgeInsets.all(4),
              padding: EdgeInsets.all(4),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.supervised_user_circle),
                    title: Text('${snapshot.data[index].nombre}'),
                    subtitle: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('MONEDERO ${snapshot.data[index].monedero} '),
                        Divider(),
                        Text('eMail: ${snapshot.data[index].eMail} '),
                        Text('clave: ${snapshot.data[index].clave} '),
                        Text('id: ${snapshot.data[index].idUsuario} '),
                      ],
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
 */
  ListView todoContenido(BuildContext context) {
    idUsuarioData();
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10),
          child: cardBuscar(),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: Card(
            child: Column(
              children: <Widget>[
                imagenTop(context),
                partOne(),
                partTwo(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Center imagenTop(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Image.asset(
          'assets/way.png',
          width: MediaQuery.of(context).size.width * 0.3,
        ),
      ),
    );
  }

  Row partTwo() {
    return Row(
      children: <Widget>[
        Expanded(
          child: FlatButton(
              color: Colors.yellow[800],
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CarteraPage(
                            usuario: widget.usuario,
                            monedero: double.parse(this.dataMonedero),
                            id: this.dataId,
                          )),
                );
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.attach_money),
                    Text(
                      'Cartera',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )),
        ),
        Expanded(
          child: FlatButton(
              color: Colors.green[400],
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CarritoPage(
                            usuario: widget.usuario,
                            monedero: double.parse(this.dataMonedero),
                            id: this.dataId,
                          )),
                );
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.add_shopping_cart),
                    Text(
                      'Carrito',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )),
        ),
      ],
    );
  }

  Row partOne() {
    return Row(
      children: <Widget>[
        Expanded(
          child: FlatButton(
              color: Colors.red[400],
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ComprarPage(
                            usuario: widget.usuario,
                            monedero: double.parse(this.dataMonedero),
                            id: this.dataId,
                          )),
                );
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.compare_arrows),
                    Text(
                      'Comprar Productos',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )),
        ),
        Expanded(
          child: FlatButton(
              color: Colors.blue[300],
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => VerderPage(
                            usuario: widget.usuario,
                            monedero: double.parse(this.dataMonedero),
                            id: this.dataId,
                          )),
                );
              },
              child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.attach_money),
                    Text(
                      'Vender Productos',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )),
        ),
      ],
    );
  }

  Card cardBuscar() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(14.0),
        child: Column(
          children: <Widget>[
            Text('Bienvenido!  '),
            Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: controller,
                    decoration: InputDecoration(
                        prefix: Text('Buscar: '),
                        helperText: 'Busca ese producto que deseas.'),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BusquedaPage(
                                usuario: widget.usuario,
                                monedero: double.parse(this.dataMonedero),
                                id: this.dataId,
                                buscar: controller.text,
                              )),
                    );
                  },
                  color: Colors.blue[400],
                  splashColor: Colors.deepOrange,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
