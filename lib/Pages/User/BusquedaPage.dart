import 'package:flutter/material.dart';
import 'package:gabs/Pages/Database.dart';
import 'package:gabs/Pages/Modelos.dart';
import 'package:toast/toast.dart';

import 'funciones.dart';

class BusquedaPage extends StatefulWidget {
  final String usuario;
  final String id;
  final double monedero;
  final String buscar;
  BusquedaPage({Key key, this.usuario, this.id, this.monedero, this.buscar})
      : super(key: key);

  @override
  _BusquedaPageState createState() => _BusquedaPageState();
}

class _BusquedaPageState extends State<BusquedaPage> {
  ScrollController controller;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: Text('Bienvenido'),
        actions: <Widget>[
          FlatButton.icon(
              onPressed: null,
              icon: Icon(Icons.person_pin),
              label: Text('${widget.usuario}')),
          FlatButton.icon(
              onPressed: null,
              icon: Icon(Icons.monetization_on),
              label: Text(numberFormat(widget.monedero).toString()))
        ],
      ),
      body: Container(
        color: Colors.indigo[50],
        child: FutureBuilder(
          future: DatabaseHelper.instance.busquedaProductos(widget.buscar),
          builder: (context, snapshot) {
            if (snapshot.data.length <= 0) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    child: Text('Contenido no encontrado'),
                  ),
                  Center(child: CircularProgressIndicator()),
                ],
              );
            }

            return Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Buscando: ${widget.buscar}.. '),
                ),
                Expanded(
                  child: ListView.builder(
                    controller: controller,
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return mostrandoContenido(snapshot, index, context);
                    },
                  ),
                ),
              ],
            );
          },
        ),

        //paraVisualizar(),
      ),
    );
  }

  Container mostrandoContenido(
      AsyncSnapshot snapshot, int index, BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4),
      padding: EdgeInsets.all(4),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          ListTile(
            leading: FadeInImage.assetNetwork(
                placeholder: 'assets/cargando.gif',
                image: '${snapshot.data[index].imagenUrl}'),
            title: Text('${snapshot.data[index].nombre}'),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('${snapshot.data[index].desc} '),
                Divider(),
                Text('${snapshot.data[index].direccion}'),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton.icon(
                        onPressed: null,
                        icon: Icon(Icons.attach_money),
                        label: Text('${snapshot.data[index].precio}')),
                    FlatButton.icon(
                        onPressed: null,
                        icon: Icon(Icons.call_to_action),
                        label: Text('${snapshot.data[index].cantidad}')),
                  ],
                )
              ],
            ),
            onTap: () {
              DatabaseHelper.instance.insertCarrito(Carrito(
                  idProducto:
                      int.parse(snapshot.data[index].idProducto.toString()),
                  idUsuario: int.parse(widget.id)));

              Toast.show("Agregado al carrito", context,
                  duration: 2, gravity: Toast.BOTTOM);
            },
            trailing: Icon(Icons.add_shopping_cart),
          ),
          snapshot.data[index].idUsuario == int.parse(widget.id)
              ? opcionesCRUD(
                  snapshot.data[index].idProducto, snapshot.data[index].nombre)
              : Text('No puedo editar este producto')
        ],
      ),
    );
  }

  Row opcionesCRUD(int idProducto, String nombre) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(
          width: 4.0,
        ),
        FlatButton.icon(
          onPressed: () {
            //snapshot.data[index].idProducto
            _showMyDialog(idProducto, nombre);
          },
          icon: Icon(Icons.delete),
          label: Text('Eliminar'),
          color: Colors.red[400],
        )
      ],
    );
  }

  Future<void> _showMyDialog(int idProducto, String nombre) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Advertencia'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('¿Desea eliminar el producto? '),
                Text(
                  '$nombre',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Si! Eliminar'),
              onPressed: () {
                DatabaseHelper.instance.deleteProducto(idProducto);
                Navigator.of(context).pop();
                Navigator.of(context).pop();
                Toast.show("Eliminado Exitosamente", context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
              },
            ),
          ],
        );
      },
    );
  }
}
