import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../DataBase.dart';
import '../Modelos.dart';
import 'funciones.dart';

class CarritoPage extends StatefulWidget {
  final String usuario;
  final String id;
  final double monedero;

  CarritoPage({Key key, this.usuario, this.id, this.monedero})
      : super(key: key);
  @override
  _CarritoPageState createState() => _CarritoPageState();
}

class _CarritoPageState extends State<CarritoPage> {
  String nombre, imagenUrl;
  double precio, suma = 0;
  int cont = 1;

  List<Carrito> listaCarritoO;
  ScrollController controller = new ScrollController();

  listaCarritoOculta(int idUsuario) async {
    var listaCarrito =
        await DatabaseHelper.instance.listaCarrito(int.parse(widget.id));
    if (this.mounted) {
      setState(() {
        listaCarritoO = listaCarrito.toList();
      });
      for (var item in listaCarritoO) {
        //print('id: Producto: ${item.idProducto}');
        listaProductosOculta(item.idProducto);
      }
    }
  }

  listaProductosOculta(int idProducto) async {
    var v = await DatabaseHelper.instance.listaProductosEspcifico(idProducto);
    if (this.mounted) {
      setState(() {
        precio = double.parse(v.last.precio.toString());
        suma = suma + precio;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    listaCarritoOculta(int.parse(widget.id));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          //title: Text('Bienvenido'),
          actions: <Widget>[
            FlatButton.icon(
                onPressed: null,
                icon: Icon(Icons.person_pin),
                label: Text('${widget.usuario}')),
            FlatButton.icon(
                onPressed: null,
                icon: Icon(Icons.monetization_on),
                label: Text(numberFormat(widget.monedero).toString()))
          ],
        ),
        body: Container(
          color: Colors.indigo[50],
          //snapshot.data[index].idProducto
          child: FutureBuilder(
            future: DatabaseHelper.instance.listaCarrito(int.parse(widget.id)),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }

              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      addAutomaticKeepAlives: true,
                      controller: controller,
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        sinc(snapshot.data[index].idProducto);

                        return cajaContenidos(context, snapshot, index);
                      },
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                            padding: EdgeInsets.all(12.0),
                            color: Colors.grey[800],
                            child: Text(
                              'Subtotal; $suma',
                              style: TextStyle(color: Colors.white),
                            )),
                      ),
                      FlatButton.icon(
                          onPressed: () {
                            /*   Navigator.pop(context); */

                            if (suma > widget.monedero) {
                              Toast.show(
                                  "No tienes fondos suficientes", context,
                                  duration: 2, gravity: Toast.BOTTOM);
                            } else {
                              Navigator.pop(context);

                              Toast.show(
                                  "¡Productos comprados con exito!", context,
                                  duration: 3, gravity: Toast.BOTTOM);

                              double cantidad = widget.monedero - suma;
                              DatabaseHelper.instance.updateMonedero(Monedero(
                                  idUsuario: int.parse(widget.id),
                                  monedero: cantidad));

                                 DatabaseHelper.instance
                                .deleteTodosCarrito(int.parse(widget.id)); 
                            }

                            
                          },
                          icon: Icon(Icons.payment),
                          color: Colors.blue[200],
                          label: Text('Comprar'))
                    ],
                  )
                ],
              );
            },
          ),
        ));
  }

  Padding cajaContenidos(
      BuildContext context, AsyncSnapshot snapshot, int index) {
    //

    //suma = suma + precio;

    /*  if(index <= (snapshot.data.length)) {

  } */
    //

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        color: Colors.white,
        child: ListTile(
          leading: FadeInImage.assetNetwork(
              placeholder: 'assets/cargando.gif', image: '$imagenUrl'),
          title: Flex(
            mainAxisAlignment: MainAxisAlignment.start,
            direction: Axis.horizontal,
            children: <Widget>[
              FlatButton.icon(
                  onPressed: null,
                  icon: Icon(Icons.blur_circular),
                  label: Text('$nombre')),
            ],
          ),
          subtitle: Flex(
            mainAxisAlignment: MainAxisAlignment.start,
            direction: Axis.horizontal,
            children: <Widget>[
              FlatButton.icon(
                  onPressed: null,
                  icon: Icon(Icons.monetization_on),
                  label: Text('Precio: $precio')),
            ],
          ),
          trailing: Icon(Icons.delete),
          onLongPress: () {
            Navigator.pop(context);
            Toast.show("Item Eliminado", context,
                duration: 2, gravity: Toast.BOTTOM);

            DatabaseHelper.instance.deleteCarrito(
                int.parse(snapshot.data[index].idCarrito.toString()));

                DatabaseHelper.instance.incrementarCantidad(
                                snapshot.data[index].idProducto);

            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CarritoPage(
                        usuario: widget.usuario,
                        monedero: widget.monedero,
                        id: widget.id.toString(),
                      )),
            );
          },
        ),
      ),
    );
  }

  sinc(int id) async {
    var v = await DatabaseHelper.instance.listaProductosEspcifico(id);
    if (this.mounted) {
      setState(() {
        nombre = v.last.nombre;
        precio = double.parse(v.last.precio.toString());
        imagenUrl = v.last.imagenUrl;
      });
    }
  }
}
