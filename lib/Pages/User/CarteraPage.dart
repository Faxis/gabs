import 'package:flutter/material.dart';
import 'package:gabs/Pages/Database.dart';
import 'package:toast/toast.dart';

import '../Modelos.dart';
import 'funciones.dart';

class CarteraPage extends StatefulWidget {
  final String usuario;
  final String id;
  final double monedero;

  CarteraPage({Key key, this.usuario, this.id, this.monedero})
      : super(key: key);

  @override
  _CarteraPageState createState() => _CarteraPageState();
}

class _CarteraPageState extends State<CarteraPage> {
  TextEditingController nTarjeta = new TextEditingController();
  TextEditingController nCodigo = new TextEditingController();
  TextEditingController nCantidad = new TextEditingController();

  double cantidad;

  List valorItem = <String>[
    'Visa',
    'MasterCard',
  ];

  String dropdownValue = 'Visa';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: Text('Bienvenido'),
        actions: <Widget>[
          FlatButton.icon(
              onPressed: null,
              icon: Icon(Icons.person_pin),
              label: Text('${widget.usuario}')),
          FlatButton.icon(
              onPressed: null,
              icon: Icon(Icons.monetization_on),
              label: Text(numberFormat(widget.monedero).toString()))
        ],
      ),
      body: Container(
        color: Colors.indigo[50],
        child: Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(4),
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.grey[800],
                ),
                child: cantidadActual(),
              ),
              mensajeText(),
              nTar(),
              filaDatos(),
              cantidadField(),
              FlatButton(
                onPressed: () {
                  if (this.nTarjeta.text.isEmpty &&
                      this.nCodigo.text.isEmpty &&
                      this.nCantidad.text.isEmpty) {
                    _showMyDialog();
                  } else {
                    cantidad = widget.monedero + double.parse(nCantidad.text);
                    DatabaseHelper.instance.updateMonedero(Monedero(
                        idUsuario: int.parse(widget.id), monedero: cantidad));

                    Toast.show("Agregado Exitosamente", context,
                        duration: 3, gravity: Toast.BOTTOM);

                    Navigator.pop(context);
                  }
                },
                child: Text('Agregar'),
                color: Colors.blue[300],
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Llena todos los campos.'),
                Text('Intentalo de nuevo'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Container cantidadField() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
          controller: nCantidad,
          obscureText: false,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            helperText: 'Cantidad para agregar a tu monedero',
            border: OutlineInputBorder(),
            labelText: 'Cantidad',
            prefixIcon: Icon(Icons.monetization_on),
          ),
        ),
      ),
    );
  }

  Row filaDatos() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: nCodigo,
                obscureText: true,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  helperText: 'Codigo de 3 Digitos (Parte de atras)',
                  border: OutlineInputBorder(),
                  labelText: 'CVC',
                  prefixIcon: Icon(Icons.code),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: DropdownButton<String>(
              hint: Text('Seleccionar Categoría'),
              isExpanded: true,
              value: dropdownValue,
              icon: Icon(Icons.arrow_downward),
              iconSize: 16,
              elevation: 2,
              style: TextStyle(color: Colors.blue[700]),
              underline: Container(
                height: 2,
                color: Colors.blue[700],
              ),
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue = newValue;
                });
              },
              items: valorItem.map<DropdownMenuItem<String>>((value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
        ),
      ],
    );
  }

  Container nTar() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
          controller: nTarjeta,
          //obscureText: true,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            helperText: 'Tu numero de tarjeta tiene 16 digitos',
            border: OutlineInputBorder(),
            labelText: 'Numero de Tarjeta',
            prefixIcon: Icon(Icons.call_to_action),
          ),
        ),
      ),
    );
  }

  Padding mensajeText() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child:
          Text('¡Agrega mas dinero a tu cuenta!', textAlign: TextAlign.center),
    );
  }

  Column cantidadActual() {
    return Column(
      children: <Widget>[
        Text(
          'Actualmente cuentas con:',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        Text(
          '\$' + numberFormat(widget.monedero).toString(),
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
      ],
    );
  }
}
