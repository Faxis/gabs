import 'package:flutter/material.dart';

class PrincipalPage extends StatefulWidget {
  PrincipalPage({Key key}) : super(key: key);

  @override
  _PrincipalPageState createState() => _PrincipalPageState();
}

class _PrincipalPageState extends State<PrincipalPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Gaps: Principal'),
        //
        backgroundColor: Colors.indigo[600],
        
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.supervised_user_circle),
              onPressed: () {
                Navigator.pushNamed(context, 'login');
              }),
          IconButton(
              icon: Icon(Icons.person_add),
              onPressed: () {
                Navigator.pushNamed(context, 'registro');
              })
        ],
      ),
      body: Container(
        color: Colors.indigo[50],
        child: ListView(
          children: <Widget>[
            logoTop(context),
            aboutUs(context),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14),
                        color: Colors.blue[300]),
                    margin: EdgeInsets.all(4),
                    padding: EdgeInsets.all(2),
                    child: ListTile(
                      subtitle: Text(
                        ' es un aplicación movil para Android con un interfaz amigable que ayuda a los usuarios a encontrar lo que necesita en los los comercios locales cerca de su ubicación.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          height: 1.5,
                        ),
                      ),
                      title: Text(
                        'Aplicación Movil',
                        textAlign: TextAlign.center,
                        style: TextStyle(height: 1.5),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14),
                        color: Colors.pink[300]),
                    margin: EdgeInsets.all(4),
                    padding: EdgeInsets.all(2),
                    child: ListTile(
                      subtitle: Text(
                        'G.A.P.S bajo la fuerza del internet quiere lograr conectar la parte digital con los negocios locales para lograr que más comercios tengan los alcances más altos como el comercio electronico.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          height: 1.5,
                        ),
                      ),
                      title: Text(
                        'Comercio Electronicol',
                        textAlign: TextAlign.center,
                        style: TextStyle(height: 1.5),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Container aboutUs(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14), color: Colors.white),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(4),
      child: ListTile(
        subtitle: Text(
          "G.A.P.S. es una aplicación móvil que ayudara a quien la use para encontrar lo que necesita, desde el articulo más sencillo como un foco, hasta lo más complicado como un tornillo, o un desarmador. Esto porque G.A.P.S. ayuda a acercar a los consumidores que buscan algo especifico a los negocios locales de la zona donde te encuentres.",
          textAlign: TextAlign.justify,
          style: TextStyle(height: 1.5),
        ),
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/head.png',
                width: MediaQuery.of(context).size.width * 0.3),
            Text('¿Quienes somos?',
                textAlign: TextAlign.center,
                style: TextStyle(height: 1.5, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }

  Container logoTop(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: Color.fromRGBO(37, 36, 41, 1)),
      child: Column(
        children: <Widget>[
          Image.asset(
            'assets/logocircle.png',
            width: MediaQuery.of(context).size.width * 0.6,
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text('La aplicación para encontrar lo que buscas',
                style: TextStyle(color: Colors.white)),
          )
        ],
      ),
    );
  }
}
