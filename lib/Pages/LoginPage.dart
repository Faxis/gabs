import 'package:flutter/material.dart';

import 'DataBase.dart';
import 'User/PrincipalUserPage.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailC = new TextEditingController();
  TextEditingController claveC = new TextEditingController();

  String no, cl;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        backgroundColor: Colors.orange[400],
      ),
      body: Container(
        color: Colors.indigo[50],
        child: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Image.asset(
                        'assets/login.png',
                        width: MediaQuery.of(context).size.width * 0.6,
                      ),
                    ],
                  ),
                  Container(
                    child: TextField(
                      controller: emailC,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'E-Mail',
                          prefixIcon: Icon(Icons.alternate_email),
                          suffixIcon: Icon(Icons.short_text)),
                    ),
                  ),
                  Divider(),
                  Container(
                    child: TextField(
                      controller: claveC,
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Clave',
                          prefixIcon: Icon(Icons.security),
                          suffixIcon: Icon(Icons.short_text)),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(20),
                    child: FlatButton(
                      child: Text('Entrar'),
                      color: Colors.blueAccent,
                      textColor: Colors.white,
                      onPressed: () {
                        print('Ingrese: ${emailC.text}');
                        print('Ingrese: ${claveC.text}');

                        buscarUser(emailC.text, claveC.text);
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  buscarUser(String email, String clave) async {
    var v = await DatabaseHelper.instance.buscarUsuarios(email, clave);

    if (v.isEmpty) {
      _showMyDialog();
    } else {
      //v.last.nombre
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                //nombre.text =
                PrincipalUserPage(usuario: v.last.nombre, eMail: v.last.eMail)),
      );
    }
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Usuario no encontrado.'),
                Text('Intentalo de nuevo'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cerrar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
