class Usuarios {
  final int idUsuario;
  final String nombre;
  final String clave;
  final String eMail;
  final double monedero;

  Usuarios(
      {this.idUsuario, this.nombre, this.clave, this.eMail, this.monedero});

  Map<String, dynamic> toMap() {
    return {
      //'id': idUsuario,
      'nombre': nombre,
      'clave': clave,
      'email': eMail,
      'monedero': monedero,
    };
  }
}

class Monedero {
  final int idUsuario;
  final double monedero;

  Monedero({this.idUsuario, this.monedero});

  Map<String, dynamic> toMap() {
    return {
      //'id': idUsuario,
      'monedero': monedero,
    };
  }
}

class Productos {
  final int idProducto;
  final String nombre;
  final String desc;
  final double precio;
  final String imagenUrl;
  final int cantidad;
  final String direccion;
  final String categoria;
  final int idUsuario;

  Productos({
    this.idProducto,
    this.nombre,
    this.desc,
    this.precio,
    this.imagenUrl,
    this.cantidad,
    this.direccion,
    this.categoria,
    this.idUsuario,
  });

  Map<String, dynamic> toMap() {
    return {
      'nombre': nombre,
      'desc': desc,
      'precio': precio,
      'imagenUrl': imagenUrl,
      'cantidad': cantidad,
      'direccion': direccion,
      'categoria': categoria,
      'idUsuario': idUsuario,
    };
  }

  String getNombre() {
    return nombre;
  }
}

class Carrito {
  final int idCarrito;
  final int idUsuario;
  final int idProducto;

  Carrito({this.idProducto, this.idUsuario, this.idCarrito});

  Map<String, dynamic> toMap() {
    return {
      'idUsuario': idUsuario,
      'idProducto': idProducto,
    };
  }

  
}
