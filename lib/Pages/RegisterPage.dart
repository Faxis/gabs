import 'package:flutter/material.dart';
import 'package:gabs/Pages/User/PrincipalUserPage.dart';

import 'DataBase.dart';
import 'Modelos.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController nombre = new TextEditingController();
  TextEditingController clave = new TextEditingController();
  TextEditingController reClave = new TextEditingController();

  TextEditingController eMail = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registro'),
        backgroundColor: Colors.blue[400],
      ),
      body: Container(
        color: Colors.indigo[50],
        child: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/register.png',
                          width: MediaQuery.of(context).size.width * 0.4,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    child: TextField(
                      controller: nombre,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          helperText: 'Escribe el nombre del usuario',
                          border: OutlineInputBorder(),
                          labelText: 'Nombre de Usuario',
                          prefixIcon: Icon(Icons.person_pin),
                          suffixIcon: Icon(Icons.short_text)),
                    ),
                  ),
                  Divider(),
                  Container(
                    child: TextField(
                      controller: eMail,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          helperText:
                              'Escribe tu correo electronico (debe ser real).',
                          border: OutlineInputBorder(),
                          labelText: 'E-Mail',
                          prefixIcon: Icon(Icons.alternate_email),
                          suffixIcon: Icon(Icons.short_text)),
                    ),
                  ),
                  Divider(),
                  inputClaveRe(),
                  Divider(),
                  inputClave(),
                  botonRegistrar(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Container inputClaveRe() {
    return Container(
      child: TextField(
        controller: clave,
        obscureText: true,
        keyboardType: TextInputType.visiblePassword,
        decoration: InputDecoration(
            helperText: 'Escribe tu clave, de 6 a 8 caracteres.',
            border: OutlineInputBorder(),
            labelText: 'Clave',
            prefixIcon: Icon(Icons.security),
            suffixIcon: Icon(Icons.short_text)),
      ),
    );
  }

  Container inputClave() {
    return Container(
      child: TextField(
        controller: reClave,
        obscureText: true,
        keyboardType: TextInputType.visiblePassword,
        decoration: InputDecoration(
            helperText: 'Repite la clave.',
            border: OutlineInputBorder(),
            labelText: 'Repetir Clave',
            prefixIcon: Icon(Icons.security),
            suffixIcon: Icon(Icons.short_text)),
      ),
    );
  }

  Container botonRegistrar() {
    return Container(
      margin: EdgeInsets.all(20),
      child: FlatButton(
        child: Text('Registrate'),
        color: Colors.blueAccent,
        textColor: Colors.white,
        onPressed: () {
          print('Se ingreso ${nombre.text}');
          print('Se ingreso ${eMail.text}');
          print('Se ingreso ${clave.text}');
          print('Se ingreso ${reClave.text}');

          if (clave.text != reClave.text) {
            _showMyDialog();
          } else {
            DatabaseHelper.instance.insertUsuario(Usuarios(
                idUsuario: null,
                nombre: nombre.text,
                eMail: eMail.text,
                clave: clave.text,
                monedero: 0));

            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      //nombre.text =
                      PrincipalUserPage(
                          usuario: nombre.text, eMail: eMail.text)),
            );
          }
        },
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Las claves no coinciden.'),
                Text('Intentalo de nuevo'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceptar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
