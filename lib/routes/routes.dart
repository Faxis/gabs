import 'package:flutter/material.dart';
import 'package:gabs/Pages/LoginPage.dart';
import 'package:gabs/Pages/PrincipalPage.dart';
import 'package:gabs/Pages/RegisterPage.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => PrincipalPage(),
    'login': (BuildContext context) => LoginPage(),
    'registro': (BuildContext context) => RegisterPage()
  };
}
